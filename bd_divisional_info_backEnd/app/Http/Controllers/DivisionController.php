<?php

namespace App\Http\Controllers;

use App\Models\Division;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DivisionController extends Controller
{
    /**
     * This Command create resource controller and model.
     * command is : # php artisan make:controller DivisionController --resource --model=Division
     */
    public function index()
    {
        //
        $data = Division::select('id','name','bng_name', 'established', 'iso', 'hdi', 'status_active', 'is_delete')
            ->orderBy('name', 'asc'
            )->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data=array();
        $data['name']=$request->name;
        $data['bng_name']=$request->bng_name;
        $data['established']=$request->established;
        $data['iso']=$request->iso;
        $data['hdi']=$request->hdi;
        $data['status_active']=1;
        $data['is_delete']=0;
        $data['created_by']=1;
        $data['update_by']=1;
        $data['created_at']=now();
        $result=DB::table('divisions')->insertGetId($data);
        return response()->json(['message' => 'Data saved successfully']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Division $division)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Division $division)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Division $division)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Division $division)
    {
        $division->delete();
        return response('Data is Delete');
    }
    public function dashboard_info()
    {
        // $data = Division::find('id')->count();
        $data = Division::select(DB::raw('COUNT(id) as div_count'))->get();
        return response()->json($data);
    }
    public function division_dropdown(){
        $data = Division::select('id','name','bng_name')
            ->orderBy('name', 'asc'
            )->get();
        return response()->json($data);
    }
}
