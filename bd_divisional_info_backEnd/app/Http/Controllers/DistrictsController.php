<?php

namespace App\Http\Controllers;

use App\Models\districts;
use App\Models\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class districtsController extends Controller
{
    /**
     * Display a listing of the resource.
     * command use php artisan make:model districts -mcr
     */
    public function index()
    {
        $data = districts::select('districts.id', 'districts.name','divisions.id as division_id', 'divisions.name as division', 'districts.bng_name', 'districts.established', 'districts.iso', 'districts.hdi', 'districts.post_code', 'districts.status_active', 'districts.is_delete')
            ->leftJoin('divisions', 'districts.division_id', '=', 'divisions.id')
            ->orderBy('division', 'asc')
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // SELECT `id`, `name`, `division_id`, `bng_name`, `established`, `iso`, `hdi`, `post_code`, `status_active`, `is_delete`, `created_by`, `created_at`, `update_by`, `update_at` FROM `districts` WHERE 1
        $data=array();
        $data['division_id']            =$request->cboDivision;
        $data['name']                   =$request->dis_name;
        $data['bng_name']               =$request->dis_bng_name;
        $data['established']            =$request->dis_established;
        $data['iso']                    =$request->dis_iso;
        $data['hdi']                    =$request->dis_hdi;
        $data['post_code']              =$request->nbrDisPostCode;
        $data['status_active']          =1;
        $data['is_delete']              =0;
        $data['created_by']             =1;
        $data['created_at']             =now();
        $result=DB::table('districts')->insertGetId($data);
        return response()->json(['message' => 'Data saved successfully']);
    }

    /**
     * Display the specified resource.
     */
    public function show(districts $districts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(districts $districts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

            $district = DB::table('districts')
                ->Select('districts.id', 'districts.name','divisions.id as division_id', 'divisions.name as division', 'districts.bng_name', 'districts.established', 'districts.iso', 'districts.hdi', 'districts.post_code', 'districts.status_active', 'districts.is_delete')
                ->join('divisions', 'districts.division_id', '=', 'divisions.id')
                ->where('districts.id', '=', $id)
                ->first();

            if ($district) {
                // Update district record
                $district->division_id = $request->input('cboDivision');
                $district->name = $request->input('dis_name');
                $district->bng_name = $request->input('dis_bng_name');
                $district->established = $request->input('dis_established');
                $district->iso = $request->input('dis_iso');
                $district->hdi = $request->input('dis_hdi');
                $district->post_code = $request->input('nbrDisPostCode');
                $district->update_by = 1;
                $district->update_at = now();
                $district->save();
                // Return success response
                return response()->json(['message' => 'District ' . $id . ' updated successfully']);
            } else {
                // Return error response if district not found
                return response()->json(['error' => 'District not found'], 404);
            }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): \Illuminate\Http\JsonResponse
    {
        DB::table('districts')->where('id',$id)->delete();

        return response()->json(['message' => 'District ' . $id . ' has been deleted']);

    }
    public function dashboard_info()
    {
        // $data = Division::find('id')->count();
        $data = districts::select(DB::raw('COUNT(id) as dis_count'))->get();
        return response()->json($data);
    }
    public function districts_dropdown(){
        $data = Division::select('id','name','bng_name')
            ->orderBy('name', 'asc'
            )->get();
        return response()->json($data);
    }
}
