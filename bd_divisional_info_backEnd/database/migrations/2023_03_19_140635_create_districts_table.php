<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('division_id');
            $table->string('bng_name')->unique();
            $table->integer('established');
            $table->string('iso');
            $table->float('hdi',8,4);
            $table->integer('post_code');

            $table->integer('status_active')->default(1);
            $table->integer('is_delete')->default(0);

            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();

            $table->integer('update_by')->nullable();
            $table->timestamp('update_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('districts');
    }
};
