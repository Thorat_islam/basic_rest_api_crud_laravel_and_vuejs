<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DivisionController;
use App\Http\Controllers\DistrictsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::apiResource('divisions', DivisionController::class);
Route::get('/divission_count', [DivisionController::class, 'dashboard_info']);
Route::get('/division_dropdown', [DivisionController::class, 'division_dropdown']);

Route::apiResource('districts', DistrictsController::class);
Route::get('/districts_count', [DistrictsController::class, 'dashboard_info']);
Route::get('/districts_dropdown', [DistrictsController::class, 'districts_dropdown']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
