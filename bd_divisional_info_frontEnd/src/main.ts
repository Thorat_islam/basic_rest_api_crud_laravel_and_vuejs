import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/main.css'

const app = createApp(App)
app.provide('rootApi', 'http://127.0.0.1:8000/api')
app.use(createPinia())
app.use(router)
app.mount('#master')
