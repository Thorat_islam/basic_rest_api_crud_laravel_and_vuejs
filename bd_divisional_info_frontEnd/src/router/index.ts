import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from "@/views/Dashboard.vue";
import all_division from "@/views/all_division.vue";
import Districts from "@/views/Districts.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Dashboard
    },
    {
      path: '/all_division',
      name: 'about',
      component: all_division
    },
    {
      path: '/districts',
      name: 'districts',
      component: Districts
    },
  ]
})

export default router
